import string
from pyExcelerator import *
import threading


w = Workbook()

def func(s,t) :

    
    ws = w.add_sheet(t)

    f = file (s, 'r')
    count = 0
    sum = 0
    ave = 0

    while True:
        line = f.readline()
        if len(line) == 0:
            break
        a = line.split()
        for i in range(0,len(a)):
            if a[i].isdigit():
                for j in range(i-1, 0, -1):
                    if a[j] == "is":
                         for x in range(i, len(a), 1):
                             if a[x] == "ms":
                                 count = count + 1
                                 ws.write(count ,1 , a[i])
                                 sum = sum + string.atof(a[i])
                             else :
                                 continue
                    else :
                        continue
            else :
                continue

    ave = sum / count
    
    ws.write(count +1, 1, ave)
    
    w.save('mini3.xls')
              
    f.close()

t1 = threading.Thread(target=func('Python_text_data1.txt', 'sheet1'))
t2 = threading.Thread(target=func('Python_text_data2.txt', 'sheet2'))
t3 = threading.Thread(target=func('Python_text_data3.txt', 'sheet3'))
t1.start()
t2.start()
t3.start()
    
